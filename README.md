# [Données de la recherche: la Science Reproductible et ses outils](https://sygefor.reseau-urfist.fr/#!/training/7201/7849/)

## Objectifs
Initiation aux méthodes et aux outils de traitement des données liés à la notion de "science reproductible". La formation sera donnée en anglais mais les questions peuvent être posées en français.

## Programme
Vicky Steeves (Bibliothécaire chargée des données de la recherche et de la reproductibilité scientifique, New York University, USA) présentera les définitions, concepts et enjeux liés à la reproductibilité scientifique. Puis elle fera la démonstration d'infrastructures et de workflows open source permettant de rendre la la recherche plus reproductible. On peut citer parmi ces outils l'Open Science Framework (https://osf.io/, ensemble de services proposés par la fondation Center for Open Science), git, Gitlab, R, Python, Jupyter Notebooks, Docker, et Reprozip (outil développé par New York University). Questions et discussions seront les bienvenues à l'issue de l'intervention ou pendant celle-ci.

## Pré-requis
Compréhension de l'anglais.

# [Research data: Reproducible Science and its tools](https://sygefor.reseau-urfist.fr/#!/training/7201/7849/)

## Objectives
Introduction to data processing methods and tools related to the concept of "reproducible science". The training will be given in English but questions can be asked in French.

## Program
In this session, Vicky Steeves (Librarian for Research Data Management and Reproducibility, New York University, USA) will give an introduction to the definitions, concepts, and theories around reproducibility. After reviewing these, she will demonstrate existing open-source infrastructure and workflows for more reproducible research. Such tools include the Open Science Framework, git, GitLab and CI, R, Python, Jupyter Notebooks, Docker, and ReproZip. Questions and discussions will follow, but are welcome throughout.

## Prerequisites
Understanding of English.
